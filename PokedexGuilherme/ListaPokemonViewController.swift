//
//  ListaPokemonViewController.swift
//  PokedexGuilherme
//
//  Created by COTEMIG on 26/05/22.
//

import UIKit

class ListaPokemonViewController: UIViewController, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pokemons.count
    }
    
    private var pokemons = [Pokemon]()
    
    
    
    @IBOutlet weak var tableView: UITableView!
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Minhacelula") as? TableViewCell{
            cell.titulo.text = pokemons[indexPath.row].nome
         
            if pokemons[indexPath.row].tipo_1 != nil{
                cell.tipo_1.text = pokemons[indexPath.row].tipo_1
                cell.tipo_1_box.isHidden = false
            }else{
                cell.tipo_1_box.isHidden = true
            }
            if pokemons[indexPath.row].tipo_2 != nil{
                cell.tipo_2.text = pokemons[indexPath.row].tipo_2
                cell.tipo_2_box.isHidden = false
            }else{
                cell.tipo_2_box.isHidden = true
            }
            cell.img.image = UIImage(named: pokemons[indexPath.row].img)
            cell.contentView.backgroundColor = hexStringToUIColor(hex: pokemons[indexPath.row].color)
            return cell
        }
        else{
            fatalError("Deu erro")
        }
    }
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        tableView.dataSource = self
        pokemons = [
            Pokemon(nome: "Bulbassauro", tipo_1: "Grama", tipo_2: "Veneno", img: "bulbassauro", color: "#48D0B0"),
            Pokemon(nome: "Ivyssauro", tipo_1: "Grama", tipo_2: "Veneno", img: "ivy2", color: "#48D0B0"),
            Pokemon(nome: "Venussauro", tipo_1: "Grama", tipo_2: "Veneno", img: "venussauro", color: "#48D0B0"),
            Pokemon(nome: "Charmander", tipo_1: "Fogo", tipo_2: nil, img: "charmander", color: "#FB6C6C"),
            Pokemon(nome: "Charmeleon", tipo_1: "Fogo", tipo_2: nil, img: "charmeleon", color: "#FB6C6C"),
            Pokemon(nome: "Charizard", tipo_1: "Fogo", tipo_2: "Voador", img: "charizard", color: "#FB6C6C"),
            Pokemon(nome: "Squirtle", tipo_1: "Grama", tipo_2: nil, img: "squirtle", color: "#77BDFE"),
            Pokemon(nome: "Wartotle", tipo_1: "Grama", tipo_2: "Veneno", img: "wartotle", color: "#77BDFE"),
            Pokemon(nome: "Blastoise", tipo_1: "Grama", tipo_2: "Veneno", img: "blastoise", color: "#77BDFE"),
            Pokemon(nome: "Pikachu", tipo_1: "Grama", tipo_2: "Veneno", img: "pikachu", color: "#FFCE4B"),
        ]
    }
    struct Pokemon{
        var nome: String
        var tipo_1: String?
        var tipo_2: String?
        var img: String
        var color: String
    }
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

