//
//  TableViewCell.swift
//  PokedexGuilherme
//
//  Created by COTEMIG on 02/06/22.
//

import UIKit

class TableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var tipo_1: UILabel!
    @IBOutlet weak var tipo_2: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var tipo_1_box: UIView!
    @IBOutlet weak var tipo_2_box: UIView!
}
